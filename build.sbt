//import sbtassembly.Plugin.AssemblyKeys._

name := "dribbble-mining"

version := "0.0.1"

scalaVersion := "2.11.6"

resolvers ++= Seq(
  Resolver.sonatypeRepo("public"),
  Resolver.sonatypeRepo("snapshots")
)

libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % "test"

libraryDependencies += "com.github.nscala-time" %% "nscala-time" % "1.8.0"

resolvers += "twttr" at "http://maven.twttr.com/"

libraryDependencies ++= Seq(
  "org.json4s" %% "json4s-jackson" % "3.2.11",
  //"com.twitter" %% "twitter-server" % "1.10.0",
  "com.twitter" %% "finagle-httpx" % "6.25.0"
  //"com.github.finagle" %% "finch-core" % "0.6.0",
  //"com.github.finagle" %% "finch-json" % "0.6.0"
)

autoCompilerPlugins := true

//assemblySettings

//jarName in assembly := s"${name.value}-${version.value}.jar"

//mergeStrategy in assembly := defaultMergeStrategy
