import java.util.logging.Logger

import com.twitter.finagle.Httpx
import com.twitter.finagle.builder.ClientBuilder
import com.twitter.finagle.httpx.{Status, Request, Http}
import com.twitter.util.{Await, Return, Future}

//import scala.concurrent.duration._
import com.twitter.conversions.time._

import scala.collection.mutable

trait Source {
  def fetchFollowers(userId: String): Future[Seq[String]]
  def fetchShots(userId: String): Future[Seq[String]]
  def fetchShotLikers(shotId: String): Future[Seq[String]]
}

trait LocalSource extends Source {
  def fetchFollowers(userId: String): Future[Seq[String]] = Future.value("1,2,3".split(","))

  def fetchShots(userId: String): Future[Seq[String]] = Future.value("10,11".split(","))
  def fetchShotLikers(shotId: String): Future[Seq[String]] = shotId match {
    case "10" =>  Future.value("1,2,4".split(","))
    case "11" =>  Future.value("1,5".split(","))
    case _ =>  Future.value(Seq.empty[String])
  }
}

object DribbbleAuth {
  lazy val accessToken = scala.io.Source.fromURL(this.getClass.getResource(".access-token")).getLines().mkString //(scala.io.Codec.default)
}

trait RemoteSource extends Source {
  import org.json4s._
  import org.json4s.jackson.JsonMethods._

  implicit val formats = DefaultFormats
  implicit val timer = com.twitter.finagle.util.DefaultTimer.twitter

  val logger = Logger.getLogger("RemoteSource")
  def accessToken = DribbbleAuth.accessToken

  val host = "api.dribbble.com"

//  val client = Httpx.client.withTlsWithoutValidation().newClient("api.dribbble.com:443")
  val client = ClientBuilder()
    .codec(Http())
    .hosts(s"$host:443")
    .hostConnectionLimit(30)
    .tcpConnectTimeout(1.second)        // max time to spend establishing a TCP connection.
    .retries(2)                         // (1) per-request retries
    .tlsWithoutValidation()
    .logger(Logger.getLogger("http"))
    .failFast(false)
    .keepAlive(true)
    .build()
  val LinkHeaderRegex = """<([^>]+)>; rel="([^"]+)"""".r
  def parseLinkHeader(line: String): Map[String, String] = {
    LinkHeaderRegex
      .findAllMatchIn(line)
      .toSeq
      .foldLeft(Map.empty[String,String]) { (acc, m) => acc + (m.group(2) -> m.group(1)) }
  }

  def fetchFollowers(userId: String): Future[Seq[String]] = foldPages(Request(s"/v1/users/$userId/followers", "access_token" -> accessToken, "per_page" -> "100")) { json =>
    json.children.map { f => (f \ "follower" \ "id").extract[Long].toString }
  }()

  def foldPages(mkRequest: => Request)(f: JValue => Seq[String]): () => Future[Seq[String]] = {
    def helper() = {
      def fold(results: Seq[String], path: Option[String] = None): Future[Seq[String]] = {
        // construct request
        val request = path.map(Request.apply).getOrElse(mkRequest) // TODO add per_page=100
        request.host = host
        logger.info("requesting " + request)
        // fetch
        val response = client(request)
        // check response is 200
        response.flatMap { rsp => rsp.status match {
          case Status.Ok =>
            logger.info("link header: " + rsp.headerMap.get("Link"))
            // try parse json
            val content = rsp.contentString
            // query user ids
            val json = parse(content)
            val ids = f(json)
            // extract next link from link header
            val nextPage = rsp.headerMap.get("Link").map(parseLinkHeader).getOrElse(Map.empty[String, String]).get("next")
            logger.info("next page: " + nextPage)

            if (nextPage.isDefined) fold(results ++ ids, nextPage)
            else Future.value(results ++ ids)

          case Status(429) =>
            logger.warning("too many requests, rate limit exceeded " + rsp.headerMap)
//            X-RateLimit-Reset: 1430735040
            rsp.headerMap.get("X-RateLimit-Reset").map { resetAt =>
              val during = resetAt.toLong * 1000L - System.currentTimeMillis()
              logger.info(s"waiting for next rate limit reset period, sleep for $during ms")
              Future.sleep(during.millis).flatMap { _ => fold(results, path) }
            } getOrElse Future.value(Seq.empty[String])

          case _ =>
            logger.warning(s"received non 200 status: $rsp")
            // handle error
            Future.value(Seq.empty[String])
        }}

      }
      fold(Seq.empty[String])
    }

    helper
  }

  def fetchShots(userId: String): Future[Seq[String]] = foldPages(Request(s"/v1/users/$userId/shots", "access_token" -> accessToken, "per_page" -> "100")) { json =>
    json.children.map { s => (s \ "id").extract[Long].toString }
  }()

  def fetchShotLikers(shotId: String): Future[Seq[String]] = foldPages(Request(s"/v1/shots/$shotId/likes", "access_token" -> accessToken, "per_page" -> "100")) { json =>
    json.children.map { s => (s \ "user" \ "id").extract[Long].toString }
  }()

}
trait MineFollowers { self: Source =>
  def topFollowers(userId: String, atMost: Int = 10): Future[Seq[(String, Int)]] = {

    def count(intersected: Seq[Future[Seq[String]]]): Future[scala.collection.Map[String, Int]] = {
      Future.collect(intersected).map { followersGrouppedByShot =>
        followersGrouppedByShot.foldLeft(mutable.Map.empty[String, Int]) { (acc, followerIds) =>
          for {f <- followerIds} acc(f) = acc.getOrElse(f, 0) + 1
          acc
        }
      }
    }

    for {
      followers <- fetchFollowers(userId)
      shots <- fetchShots(userId)
      intersected = shots.map { sid => fetchShotLikers(sid).map { likers => followers intersect likers}}
      counters <- count(intersected)
    } yield counters.toVector.sortBy( - _._2).take(atMost)

  }
}

object Main {

  val userId = "designomatt"
  //val miner = new MineFollowers with LocalSource
  val miner = new MineFollowers with RemoteSource

  val top = miner.topFollowers(userId)
  Await.result(top)
  top.foreach(println)
}
